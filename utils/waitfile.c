#include <stdlib.h>
#include <stdio.h>

#include <library.h>

int main(int argc, char *argv[]){
    if(argc<2){
        fprintf(stderr,"Filename missing!\n");
        return 7; /* Missing Argument */
    }else{
        waitfile(argv[1]);
        return 0;
    }
    return 1; /* Unknown error */
}

