#define SLEEP_TIME 300
#define MAX_RESPAWN_LIMIT 10
/* service functions */
int init_mount();
int run_service(char* name);

/* wait until file exists */
int waitfile(char *fname);

/* respawn command if exited */
int respawn(char *cmd);

/* reboot and poweroff*/
void reboot();
void poweroff();
