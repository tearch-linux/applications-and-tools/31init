#define _GNU_SOURCE
#include <unistd.h>
#include <library.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mount.h>
#include <stddef.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>

void init_mount(){
    mount("", "/", "", MS_REMOUNT, NULL);
    mount("devtmpfs", "/dev", "devtmpfs", 0, NULL);
    mount("devpts", "/dev/pts", "devpts", 0, NULL);
    mount("sysfs", "/sys", "sysfs", 0, NULL);
    mount("proc", "/proc", "proc", 0, NULL);
    mount("tmpfs", "/run", "tmpfs", 0, NULL);
    mkdir("/run/31", 0700);
}

int run_service(char* name){
    char* service;
    char* create_path;
    asprintf(&service,"/etc/31.d/%s",name);
    if(opendir(service) != NULL)
        return 0;
    fprintf(stderr,"Start service: %s\n",name);
    int i = system(service);
    if(i != 0){
        fprintf(stderr,"Failed to start service: %s\n",name);
        fflush(stderr);
    }else{
        asprintf(&create_path,"/run/31/%s",name);
        FILE *sfile = fopen(create_path,"w");
        fprintf(sfile,"\n");
        fclose(sfile);
    }
    return i;
}

/* wait until file exists */
int waitfile(char *fname){
    while(1){
       if(access( fname, F_OK ) == 0 ){
           return 0; /* Found */
       }
       usleep(SLEEP_TIME*1000);
    }
}

int respawn(char *cmd){
    int i = 0;
    while(1){
        system(cmd);
        usleep(SLEEP_TIME*1000);
        i = i + 1;
        if(i>=MAX_RESPAWN_LIMIT){
            return 0;
        }
    }
}

void reboot(){
    FILE *sysrq = fopen("/proc/sys/kernel/sysrq","w");
    FILE *trigger = fopen("/proc/sysrq-trigger","w");
    fprintf(sysrq,"1");
    fflush(sysrq);
    fclose(sysrq);
    fprintf(trigger,"s");
    fflush(sysrq);
    fprintf(trigger,"b");
    fflush(sysrq);
    fclose(trigger);
}

void poweroff(){
    FILE *sysrq = fopen("/proc/sys/kernel/sysrq","w");
    FILE *trigger = fopen("/proc/sysrq-trigger","w");
    fprintf(sysrq,"1\n");
    fclose(sysrq);
    fprintf(trigger,"s\n");
    fprintf(trigger,"o\n");
    fclose(trigger);
}
