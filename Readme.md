# 31 init
31 line init system written C

## Building:
`make` && `make install DESTDIR=/ SBINDIR=/sbin`

## Services
Services are stored in **/etc/31.d** and started alphabeticaly.

## Components

* 31          : init manager
* 31-reboot   : reboot system
* 31-poweroff : poweroff system 

## Using sysv-init services
You can create symlink like this:
`ln -s ../31.d/xx-service /etc/init.d/example`
**xx** is number for ordering.
