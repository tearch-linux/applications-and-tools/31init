DESTDIR=/
SBINDIR=/sbin
LIBDIR=/lib
INCLUDEDIR=/usr/include
CC=gcc
build:
	$(CC) -o 31 init.c utils/library.c -I utils $(CFLAGS) -static
	@make -C utils
install:
	mkdir -p $(DESTDIR)/$(SBINDIR)  $(DESTDIR)/etc/31.d || true
	install 31 $(DESTDIR)/$(SBINDIR)/31-init
	install inits/* $(DESTDIR)/etc/31.d
	@make install -C utils
	install 31.begin $(DESTDIR)/etc/
clean:
	[ -f 31 ] && rm -f 31 || true
	@make clean -C utils
