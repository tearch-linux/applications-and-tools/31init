#define _GNU_SOURCE
#include <dirent.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <library.h>
struct dirent **namelist;
// main function without return
void main(){
    if(getpid() != 1)
        exit(31);
    // Starting 31.begin
    unsetenv("LANG");
    setenv("PATH","/bin:/usr/bin",1);
    if(0 != system("/bin/sh /etc/31.begin"))
        while(1); // Pause if 31.begin failed
    // Mount starting
    init_mount();
    // Mount done. Now starting init
    int n = scandir("/etc/31.d", &namelist, 0, alphasort);
    for(int i=1; i<n ;i++) {
        // fetch service name
        char *name = namelist[i]->d_name;
        // run service
        run_service(name);
    }
    // start rc.local
    system("/bin/sh /etc/rc.local");
    // infinitive wait
    while(1);
}

